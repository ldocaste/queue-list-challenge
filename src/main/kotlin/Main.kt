import kotlinx.serialization.*
import kotlinx.serialization.json.*
import java.io.File

val format = Json{ ignoreUnknownKeys = true }

// The following classes are fitting the JSON file : "data" contains "album" that contains a list of "tracks".
// They are used to deserialize the file and to get the Track objects here.
@Serializable
class Data(val data: Album)
@Serializable
class Album(val album: TrackList)
@Serializable
class TrackList(val tracks: List<Track>)

// Media is a class that represents the "media" value in the JSON file
@Serializable
class Media(val id: Int)

// Track is a data class that will get the data from the JSON file
@Serializable
data class Track(val id: Int, val artist: String, val title: String, val duration: Int, val media: Media)

// QueueList is the main class of the level 1
class QueueList {
    // queue is the main argument : it's a MutableList of Tracks so we can modify it easily
    private var queue:MutableList<Track> = mutableListOf()
    // currentTrack represents the index of the Track currently played
    private var currentTrack:Int = 0
    
    // Accessors
    fun getQueue() = queue
    fun getCurrentTrack() = currentTrack
    // Next is a function that increases currentTrack if possible
    fun next() {
        if (currentTrack < queue.lastIndex) currentTrack++
        else println("There isn't any next track.")
    }
    // Previous is a function that decreases currentTrack if possible
    fun previous() {
        if (currentTrack > 0) currentTrack--
        else println("There isn't any previous track.")
    }
    // Add is a function that adds a Track at the end of queue, the MutableList
    fun add(track: Track) {
        queue.add(track)
    }
    // Add is overloaded in order to add several tracks at the end of queue, if those tracks are in a Kotlin List
    fun add(tracks: List<Track>) {
        for (t in tracks) {queue.add(t)}
    }
    // RemoveAt is a function that removes a Track at a specific index
    fun removeAt(index: Int) {
        if (index < queue.size) queue.removeAt(index)
        else println("Can't remove the element : wrong index.")
    }
    // RemoveAt is overloaded in order to be used with a Track object and not only an index
    fun removeAt(track: Track) {
        removeAt(queue.indexOf(track))
    }
}


fun main() {
    /*
    println("------Test of the Track Class------")
    var Track1 = Track(id = 916409, artist = "Eminem", title = "Curtains Up (Skit)", duration = 29, Media(id = 916409))
    var Track2 = Track(id = 916412, artist = "Eminem", title = "White America", duration = 324, Media(id = 916412))
    var Track3 = Track(id = 916413, artist = "Eminem", title = "Business", duration = 251, Media(id = 916413))
    println(Track1)
    println("-----------------------------------")
    println("----Test of the QueueList Class----")
    println("-----------Queue Accessor----------")
    var QL = QueueList()
    println(QL.getQueue())
    println("-------CurrentTrack Accessor-------")
    println(QL.getCurrentTrack())
    println("----------------Next---------------")
    QL.next()
    println(QL.getCurrentTrack())
    println("--------------Previous-------------")
    QL.previous()
    println(QL.getCurrentTrack())
    println("----------------Add----------------")
    QL.add(Track1)
    println(QL.getQueue())
    QL.add(listOf(Track2, Track3))
    println(QL.getQueue())
    println("-------------Remove At-------------")
    QL.removeAt(5)
    println(QL.getQueue())
    QL.removeAt(1)
    println(QL.getQueue())
    QL.removeAt(Track1)
    println(QL.getQueue())
     */

    println("\n----------Deserialization----------\n")

    val jsonFile = File("data/tracks.json").readText(Charsets.UTF_8)
    val TL = format.decodeFromString<Data>(jsonFile)
    println("Importing Eminem's Album from the JSON File : ${TL.data.album.tracks}")

    println("\n---------Adding Queue List---------\n")

    var QL = QueueList()
    println("Initializing the QueueList : ${QL.getQueue()}")
    QL.add(TL.data.album.tracks[3])
    println("Adding it the 4th track : ${QL.getQueue()}")
    QL.add(listOf(TL.data.album.tracks[9], TL.data.album.tracks[11]))
    println("Adding it the 10th and 12th tracks : ${QL.getQueue()}")
    println("Current track : ${QL.getCurrentTrack()} : ${QL.getQueue()[QL.getCurrentTrack()].artist} - ${QL.getQueue()[QL.getCurrentTrack()].title}")

    println("\n---------Next and Previous---------\n")

    for (i in 0..2){
        println("Next track")
        QL.next()
        println("Current track : ${QL.getCurrentTrack()} : ${QL.getQueue()[QL.getCurrentTrack()].artist} - ${QL.getQueue()[QL.getCurrentTrack()].title}")
    }
    for (i in 0..2){
        println("Previous track")
        QL.previous()
        println("Current track : ${QL.getCurrentTrack()} : ${QL.getQueue()[QL.getCurrentTrack()].artist} - ${QL.getQueue()[QL.getCurrentTrack()].title}")
    }

    println("\n---------Remove Queue List---------\n")

    println("QueueList : ${QL.getQueue()}")
    QL.removeAt(0)
    println("Removing the first track : ${QL.getQueue()}")
    QL.removeAt(1)
    println("Removing the second lasting track : ${QL.getQueue()}")
    QL.removeAt(0)
    println("Removing the last track : ${QL.getQueue()}")

    println("\n------Adding the whole album-------\n")

    QL.add(TL.data.album.tracks)
    println("QueueList : ${QL.getQueue()}")
}